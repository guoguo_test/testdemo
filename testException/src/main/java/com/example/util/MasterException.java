package com.example.util;

public class MasterException extends RuntimeException{
    public MasterException(String message) {
        super(message);
    }
}
