package com.example.service;

import com.example.dao.OrderMapper;
import com.example.demo.DemoApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = DemoApplication.class)
class OrderServiceImplTest {

    @Autowired
    private OrderMapper orderMapper;

    @Test
    void addOrder() {
        for (int i = 1; i < 11; i++) {
            int id = i;
            String name = "测试" + i;
            String type = "订单";
            orderMapper.addOrder(id, name, type);
        }
    }
}