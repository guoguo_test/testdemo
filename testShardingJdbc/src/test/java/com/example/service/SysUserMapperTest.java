package com.example.service;

import com.example.dao.SysUserMapper;
import com.example.demo.DemoApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = DemoApplication.class)
class SysUserMapperTest {

    @Autowired
    private SysUserMapper sysUserMapper;
    @Test
    void addUser() {
        for (int i = 1; i < 11; i++) {
            int age = i;
            String name = "测试" + i;
            sysUserMapper.addUser(name, age);
        }
    }
}