package com.example.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

public interface OrderMapper {

    @Insert("INSERT INTO `t_order` VALUES (#{id}, #{name}, #{type}, NOW())")
    int addOrder(@Param("id") int id, @Param("name") String name, @Param("type") String type);
}
