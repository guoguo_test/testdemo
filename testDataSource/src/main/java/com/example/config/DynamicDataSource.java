package com.example.config;

import com.alibaba.druid.pool.ha.selector.StickyDataSourceHolder;
import org.apache.naming.factory.DataSourceLinkFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.Map;

/**
 * 动态数据源，继承自AbstractRoutingDataSource
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
    /**
     * 返回需要使用的数据源key，将会按照这个key从Map中获取对应的数据源（切换）
     * @return
     */
    @Override
    protected Object determineCurrentLookupKey() {
        //从ThreadLocal中取出key
        return DataSourceHolder.getDataSource();
    }
}
