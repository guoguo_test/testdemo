package com.example.demo;

import org.apache.tomcat.util.threads.ThreadPoolExecutor;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class ThreadPoolDemo {
    /**
     * 1、corePoolSize 核心线程数
     * 2、maximumPoolSize 最大线程数 任务队列存放的任务达到队列容量的时候，同时运行的最大线程数量
     * 3、workQueue 新的任务进来的时候，先判断当前运行的线程数是否达到核心线程数，如果达到，则会被放入队列中
     * 4、keepAliveTime 线程池中的线程数量>核心线程数，如果这时未有新任务提交，非核心线程不会立即销毁，而是会等待，直到等待时间超过了keepAliveTime才会被回收
     * 5、unit keepAliveTime 参数的时间单位。
     * 6、threadFactory 线程工厂 用来创建线程
     * 7、handler 饱和策略：
     *     ThreadPoolExecutor.AbortPolicy：抛出异常RejectedExecutionException拒绝执行新任务
     *     ThreadPoolExecutor.CallerRunsPolicy：直接在调用execute方法的线程中运行(run)被拒绝的任务，如果执行程序已关闭，则会丢弃该任务
     *     ThreadPoolExecutor.DiscardPolicy：不处理新任务，直接丢弃掉
     *     hreadPoolExecutor.DiscardOldestPolicy：此策略将丢弃最早的未处理的任务请求
     *
     */
//        ExecutorService executorService = Executors.newCachedThreadPool();

    /**
     * TimeUnit.DAYS          //天
     * TimeUnit.HOURS         //小时
     * TimeUnit.MINUTES       //分钟
     * TimeUnit.SECONDS       //秒
     * TimeUnit.MILLISECONDS  //毫秒
     * TimeUnit.NANOSECONDS   //毫微秒
     * TimeUnit.MICROSECONDS  //微秒
     */
    /**
     * 使用Executor启动线程比new Thread的start方法更好
     * 1、可以用线程池来实现，节约开销
     */
    /**
     * Executor组成的三大部分
     * 1、任务（Runnable/Callable）
     * 2、任务的执行
     * 3、异步计算的结果
     *
     */
    /**
     * 1、线程池首先会创建核心线程数的线程来处理任务
     * 2、如果当前的线程数大于或者等于核心线程数，则新进来的任务，则放入队列
     * 3、队列如果达到最大容量的时候，执行任务的线程数会等于最大线程数maximumPoolSize吗？
     * @param args
     */
    public static void main(String[] args) {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                5,
                10,
                1L,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(3),
                new ThreadPoolExecutor.CallerRunsPolicy());

        for (int i = 1; i <= 10; i++) {
            //1、创建实现Runnable或者Callable接口的对象
            Runnable  work= new MyRunnable("" + i);
            //调用ExecutorService.execute执行任务
            executor.execute(work);
        }
        //终止线程池
        executor.shutdown();
    }
}
