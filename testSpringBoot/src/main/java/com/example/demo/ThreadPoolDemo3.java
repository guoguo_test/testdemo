package com.example.demo;


import org.apache.tomcat.util.threads.ThreadPoolExecutor;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadPoolDemo3 {
    public static void main(String[] args) {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                5,
                10,
                1L,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(3),
                new ThreadPoolExecutor.CallerRunsPolicy());
        Executors.newCachedThreadPool();
        for (int i = 1; i <= 10; i++) {
            //1、创建实现Runnable或者Callable接口的对象
            Runnable  work= new MyRunnable("" + i);
            //2、调用ThreadPoolExecutor.execute执行任务
            executor.execute(work);
        }
        //3、终止线程池
        executor.shutdown();
    }
}

