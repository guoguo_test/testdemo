package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
public class TestThreadController {
    @GetMapping(value = "/testThread")
    void testThread() throws InterruptedException {
        System.out.println("线程名称："+Thread.currentThread().getName());
    }

/*    private void executeInternal(Runnable command) {
        if (command == null) {
            throw new NullPointerException();
        } else {
            int c = this.ctl.get();
            //1、如果当前运行的线程数小于核心线程数，那么就会新建一个线程来执行任务
            if (workerCountOf(c) < this.corePoolSize) {
                //addWorker 新建线程，并将任务添加到该线程中，启动该线程执行任务
                if (this.addWorker(command, true)) {
                    return;
                }

                c = this.ctl.get();
            }

            //2.如果当前运行的线程数等于或大于核心线程数，但是小于最大线程数，那么就把该任务放入到任务队列里等待执行
            //isRunning 判断线程池状态，线程池处于 RUNNING 状态并且队列可以加入任务，该任务才会被加入进去
            if (isRunning(c) && this.workQueue.offer(command)) {
                int recheck = this.ctl.get();
                //再次获取线程池状态，如果线程池状态不是 RUNNING 状态就需要从任务队列中移除任务，并尝试判断线程是否全部执行完毕。同时执行拒绝策略
                if (!isRunning(recheck) && this.remove(command)) {
                    this.reject(command);
                } else if (workerCountOf(recheck) == 0) {
                 // 如果当前工作线程数量为0，新创建一个线程并执行。
                    this.addWorker((Runnable)null, false);
                }
            } else if (!this.addWorker(command, false)) {
                this.reject(command);
            }

        }
    }*/
}
